# Django rest-api demo project 

### This demo is used for enabling auto test and api as document in Django rest framework

### Scope
- 1. Unit test for Django, including mock(WIP)
- 2. API test for Django(WIP)
- 3. Django rest framework Swagger(TBD)


### python package dependencies management:

```shell
# 保存當前工程的包依賴（未添加新包時跳過，環境未搭建好時請勿執行此命令）
pip freeze > requirements.txt
# 根據工程依賴包目錄一次性安裝所需的包依賴
pip install -r requirements.txt
```

### Unit test running
```shell
# 運行所有測試用例
python manage.py test
# 運行指定app的測試用例，如demo1
python manage.py test demo1
# 運行測試用例後查看運行結果，如果有失敗case，需確認並修復代碼邏輯和測試用例
```

### Unit test coverage checking commands:

```shell 
# 僅計算python工程本身的覆蓋率，不考慮調用的第三方包
coverage run --source '.' manage.py test -v 2
# 在當前控制台輸出覆蓋率總結報告
coverage report
# 生成覆蓋率匯總報告html版本，在當前目錄的htmlcov文件夾下，打開index.html查看
coverage html
```


### python unittest.mock official document:
<https://docs.python.org/3/library/unittest.mock.html>
<https://docs.python.org/3/library/unittest.mock-examples.html>
