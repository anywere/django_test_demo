from rest_framework import status
from rest_framework.test import APITestCase
from django.contrib.auth.models import User
from .models import Snippet
import json


class SnippetTests(APITestCase):
    fixtures = ['demo2_2.json']

    # 准备测试环境，创建测试用户，测试数据
    def setUp(self) -> None:
        self.user1 = dict(username='demoer', email='', password='demo1234')
        User.objects.create_superuser(**self.user1)
        self.client.login(**self.user1)

        self.snippet1 = dict(code='pass', title='default_code')
        Snippet.objects.create(**self.snippet1)

        self.url = '/demo2/snippets/'
        self.data = {
            'code': 'print("good")',
            'title': 'good_demo'
        }
        self.invalid_data = {
            'code': '',
            'title': 'good_demo'
        }
        # post 完成之后的测试数据库中记录条数（及最大主键值）
        self.post_num = 3 + 2

    def test_get_snippet(self) -> None:
        # /demo2/snippets/ GET
        response = self.client.get(self.url)
        response_content = json.loads(response.content)
        # print(response_content)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(4, len(response_content))
        self.assertEqual('first_code', response_content[0]['title'])
        # self.assertEqual('default_code', response_content[0]['title'])

    def test_create_snippet(self) -> None:
        # /demo2/snippets/ POST
        response = self.client.post(self.url, self.data)
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(Snippet.objects.count(), self.post_num)
        self.assertEqual(Snippet.objects.get(id=self.post_num).title, self.data['title'])

        # /demo2/snippets/:id GET 检查新增用户是否符合预期
        target_url = self.url + str(self.post_num) + '/'
        response = self.client.get(target_url)
        response_content = json.loads(response.content)
        # print(response_content)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(self.data['title'], response_content['title'])
        self.assertEqual(self.data['code'], response_content['code'])

    def test_post_invalid_snippet(self) -> None:
        response = self.client.post(self.url, self.invalid_data)
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    def test_get_snippet_detail_with_not_existing_pk(self) -> None:
        self.not_existing_pk = self.post_num + 1
        target_url = self.url + str(self.not_existing_pk) + '/'
        response = self.client.get(target_url)
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)



# from rest_framework.test import APIRequestFactory
#
# class SnippetListTest(TestCase):
#     def setUp(self) -> None:
#         self.client = APIClient()
#         self.client.login(username='autotest', password='test1234')
#         # self.factory = APIRequestFactory()
#         # self.request = self.factory.post('/notes/', {'title': 'new idea'})
#
