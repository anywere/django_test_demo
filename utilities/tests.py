from rest_framework.test import APITestCase
from rest_framework import status
from utilities.date_pkg import DateConverter, ts_to_date
from utilities.ext_net_request import WebRequest, get_resp, get_resp_cnt, func_input
from unittest.mock import patch, Mock
import responses


# Mock一个函数
class TestDateFunc(APITestCase):
    def setUp(self) -> None:
        self.date_str = '2020-06-02'
        self.date_ts_str = '1591056000'
        self.date_ts_int = 1591056000

    def test_ts_to_date(self) -> None:
        self.assertEqual(ts_to_date(self.date_ts_int), self.date_str)
        self.assertEqual(ts_to_date(self.date_ts_str), self.date_str)

    # 指定函数的写法：要先写当前文件名，即module name
    @patch('utilities.date_pkg.date_to_ts')
    def test_date_to_ts(self, mock_date_to_ts) -> None:
        mock_date_to_ts.return_value = 666
        self.assertEqual(mock_date_to_ts(self.date_str), 666)


class DateConverterTests(APITestCase):
    # 准备测试环境，测试数据
    def setUp(self) -> None:
        self.test_date = '2020-06-02'
        self.yes_date = '2020-06-01'
        self.month_start = '2020-06-01'
        self.year_start = '2020-01-01'

    # 测试用例执行之后的环境清理
    def tearDown(self) -> None:
        pass

    def test_get_yesterday(self) -> None:
        self.assertEqual(self.yes_date, DateConverter.get_yesterday(self.test_date))
        # self.assertEqual('2020-06-03', DateConverter.get_yesterday(self.test_date))

    # Mock一个对象里面的方法
    @patch.object(DateConverter, 'get_week_start')
    def test_week_start(self, mock_week_start) -> None:
        # 模拟add函数的返回值为年初，所以不管真实结果是否是年初，都没有影响
        mock_week_start.return_value = self.year_start
        self.assertEqual(DateConverter.get_week_start(self.test_date), self.year_start)

    def test_month_start(self) -> None:
        self.assertEqual(DateConverter.get_week_start(self.test_date), self.month_start)


class GetDemoRespTest(APITestCase):
    # 准备测试环境，测试数据
    def setUp(self) -> None:
        self.success_json = {
            "count": 4,
            "result": ['apple', 'ball', 'cat', 'dog']
        }

        self.fail_json = {
            "count": 0,
            "result": []
        }
        self.target_url = 'http://www.demo.com'

    @responses.activate
    def test_get_demo_resp_fail(self):
        responses.add(responses.GET,
                      self.target_url,
                      status=status.HTTP_400_BAD_REQUEST)
        self.assertEqual(get_resp(self.target_url).status_code, status.HTTP_400_BAD_REQUEST)

    @responses.activate
    def test_get_demo_resp_cnt_success(self):
        responses.add(responses.GET,
                      self.target_url,
                      json=self.success_json,
                      status=status.HTTP_200_OK)
        self.assertEqual(get_resp(self.target_url).status_code, status.HTTP_200_OK)
        self.assertEqual(get_resp_cnt(self.target_url), 4)

    @responses.activate
    def test_get_demo_resp_cnt_fail(self):
        responses.add(responses.GET,
                      self.target_url,
                      json=self.fail_json,
                      status=status.HTTP_200_OK)
        self.assertEqual(get_resp(self.target_url).status_code, status.HTTP_200_OK)
        self.assertEqual(get_resp_cnt(self.target_url), 0)


class InputFuncTest(APITestCase):
    # 准备测试环境，测试数据
    def setUp(self) -> None:
        self.test_name = 'Tester'
        self.test_sentence = 'Your name is Tester'

    def test_func_input(self):
        with patch('builtins.input') as mocked_input:
            # 默认的Mock输入设置
            mocked_input.side_effect = (self.test_name,)
            with patch('builtins.print') as mocked_print:
                func_input()
                # 由于 mocked_print 为 MagicMock 类型，下述已注释的测试因类型不符无法通过
                # self.assertEqual(self.test_sentence, mocked_print)
                mocked_print.assert_called_with(self.test_sentence)


class WebRequestTests(APITestCase):
    # 准备测试环境，测试数据
    def setUp(self) -> None:
        self.web_request = WebRequest()

    def test_visit_demo_success(self):
        self.web_request.get_request_status = Mock(return_value=status.HTTP_200_OK)
        self.assertEqual(self.web_request.visit_demo(), status.HTTP_200_OK)

    def test_visit_demo_fail(self):
        self.web_request.get_request_status = Mock(return_value=status.HTTP_404_NOT_FOUND)
        self.assertEqual(self.web_request.visit_demo(), status.HTTP_404_NOT_FOUND)
