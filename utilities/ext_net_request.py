import requests
import json


def get_resp(url):
    return requests.get(url)


def get_resp_cnt(url):
    resp = get_resp(url)
    resp_json = json.loads(resp.content)
    return resp_json['count']


def func_input():
    name = input("Enter your name: ")
    print('Your name is {}'.format(name))


class WebRequest:
    @staticmethod
    def get_request_status(url):
        resp = requests.get(url)
        return resp.status_code

    def visit_demo(self):
        return self.get_request_status('http://www.demo.com')
