import datetime
import calendar
import time

MONTH_FORMAT = '%Y-%m'
DATE_FORMAT = '%Y-%m-%d'
TIME_FORMAT = '%Y-%m-%d %H:%M:%S'


def ts_to_date(ts_str) -> str:
    if isinstance(ts_str, int):
        return datetime.datetime.fromtimestamp(ts_str).strftime(DATE_FORMAT)
    else:
        return datetime.datetime.fromtimestamp(int(ts_str)).strftime(DATE_FORMAT)


def date_to_ts(date_str) -> int:
    return calendar.timegm(time.strptime(date_str, DATE_FORMAT))


class DateConverter:
    @staticmethod
    def get_month_start(date_str):
        return date_str[0:8] + '01'

    @staticmethod
    def get_week_start(date_str):
        curr_date = datetime.datetime.strptime(date_str, DATE_FORMAT)
        week_delta = curr_date.isoweekday() - 1
        if week_delta == 0:
            return date_str
        else:
            week_start = curr_date - datetime.timedelta(days=week_delta)
            return week_start.strftime(DATE_FORMAT)

    @staticmethod
    def get_yesterday(date_str):
        yesterday = datetime.datetime.strptime(date_str, DATE_FORMAT) - datetime.timedelta(days=1)
        return yesterday.strftime(DATE_FORMAT)

    @staticmethod
    def get_tomorrow(date_str):
        tomorrow = datetime.datetime.strptime(date_str, DATE_FORMAT) + datetime.timedelta(days=1)
        return tomorrow.strftime(DATE_FORMAT)

    @staticmethod
    def get_7_days_later(date_str):
        days_later = datetime.datetime.strptime(date_str, DATE_FORMAT) + datetime.timedelta(days=7)
        return days_later.strftime(DATE_FORMAT)

    @staticmethod
    def get_month_end(date_str):
        month_range = calendar.monthrange(int(date_str[0:4]), int(date_str[5:7]))
        return date_str[0:8] + str(month_range)

    # return the 1 st date of month of the day 6 months ago
    @staticmethod
    def get_6_month_ago(date_str):
        month_start = date_str[0:8] + '01'
        day_before = datetime.datetime.strptime(month_start, DATE_FORMAT) - datetime.timedelta(days=180)
        return day_before.strftime(DATE_FORMAT)[0:8] + '01'

    @staticmethod
    def get_last_7_month_list(date_str):
        month_start = date_str[0:8] + '01'
        result_list = []
        for i in range(6, 0, -1):
            target_date = datetime.datetime.strptime(month_start, DATE_FORMAT) - datetime.timedelta(days=28 * i)
            result_list.append(target_date.strftime(MONTH_FORMAT))
        result_list.append(date_str[0:7])
        return result_list
