from rest_framework import status
from rest_framework.test import APITestCase
from django.contrib.auth.models import User
import json


class UserTests(APITestCase):
    # 准备测试环境，创建测试用户，测试数据
    def setUp(self) -> None:
        self.user1 = dict(username='demoer', email='', password='demo1234')
        User.objects.create_superuser(**self.user1)
        self.client.login(**self.user1)
        self.user2 = dict(username='autotester', email='', password='test1234')
        User.objects.create_superuser(**self.user2)

        self.user_list = ['demoer', 'autotester']
        self.user_url = '/demo1/users/'
        self.data = {
            "username": "autodemo",
            "email": "auto@demo.com"
        }

    def test_get_all_user(self) -> None:
        response = self.client.get(self.user_url)
        response_content = json.loads(response.content)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(2, response_content["count"])
        self.assertIn(response_content["results"][0]["username"], self.user_list)
        self.assertIn(response_content["results"][1]["username"], self.user_list)

    def test_create_user(self) -> None:
        # /demo1/users/ POST
        response = self.client.post(self.user_url, self.data)
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(User.objects.count(), 3)
        self.assertEqual(User.objects.get(id=3).username, self.data['username'])

        # /demo1/users/:id GET 地址
        response_content = json.loads(response.content)
        target_url = response_content["url"]

        # /demo1/users/:id GET 检查新增用户是否符合预期
        response = self.client.get(target_url)
        response_content = json.loads(response.content)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(self.data['username'], response_content['username'])
        self.assertEqual(self.data['email'], response_content['email'])
